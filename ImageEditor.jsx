import React, {Component} from 'react';
import ReactDOM from "react-dom";
import {connect} from 'react-redux';
import {fabric} from 'fabric';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
// import FontIcon from 'material-ui/FontIcon';
// import IconButton from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import Card, { CardActions, CardHeader, CardContent } from 'material-ui/Card';
import Toolbar , { ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import Paper from 'material-ui/Paper';
import { Reactor, Store, toImmutable } from 'nuclear-js';
import keyMirror from 'keymirror';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import { CircularProgress } from 'material-ui/Progress';
import TextField from 'material-ui/TextField';
import _ from 'lodash';

// console.log("lodash", _.bind);
// console.log(fabric)
var reactor = new Reactor({ debug: false });
var keys = keyMirror({ fabricData: null, activeObject: null });
var fabricCanvas = new fabric.Canvas();


// A place to put fabric data
var fabricStore = Store({
  getInitialState() {
    return toImmutable({
        fabricData: {
        objects: [],
      },
      activeObject: false,
    })
  },
  initialize() {
    this.on(keys.fabricData, this.saveFabricData)
    this.on(keys.activeObject, this.saveActiveObject)
  },
  saveFabricData(state, fabricData) {
        return state.set('fabricData', toImmutable(fabricData));
    },
  saveActiveObject(state, value) {
        return state.set('activeObject',value);
    }
});


function findByClipName(name) {
    // console.log("lodash search = ", name);
    // var Things = fabricCanvas.getObjects();
    // for (var i = Things.length - 1; i >= 0; i--) {
    //     if (Things[i].clipName == name){
    //         console.log( "lodash found", Things[i]);
    //         return Things[i];
    //     }
    // }
    return _(fabricCanvas.getObjects()).filter({
            clipFor: name
        }).first()
}

function degToRad(degrees) {
    return degrees * (Math.PI / 180);
}

function clipByName (ctx) {
    this.setCoords();
    var clipRect = findByClipName(this.clipName);
    var scaleXTo1 = (1 / this.scaleX);
    var scaleYTo1 = (1 / this.scaleY);
    ctx.save();

    var ctxLeft = -(this.width / 2) + clipRect.strokeWidth;
    var ctxTop = -(this.height / 2) + clipRect.strokeWidth;
    var ctxWidth = clipRect.width - clipRect.strokeWidth;
    var ctxHeight = clipRect.height - clipRect.strokeWidth;

    ctx.translate(ctxLeft, ctxTop);
    ctx.rotate(this.angle * -1 * Math.PI / 180);
    ctx.scale(scaleXTo1, scaleYTo1);

    var x = this.canvas.viewportTransform;

    ctx.scale(1 / x[0], 1 / x[3]);
    ctx.translate(x[4], x[5]);

    var scaleX = this.canvas.viewportTransform[0];
    var scaleY = this.canvas.viewportTransform[3];

    ctx.beginPath();

    ctx.rect(
    x[0] * clipRect.left - this.oCoords.tl.x,
    x[3] * clipRect.top - this.oCoords.tl.y,
    x[0] * clipRect.width*clipRect.scaleX,
    x[3] * clipRect.height*clipRect.scaleY);
    ctx.closePath();
    ctx.restore();
}


class Fabric extends React.Component{
    constructor(props){
        super(props);
        this.state={
            cropMode:false,
            xIncrease:0,
            yIncrease:0,
        }
    }

    resizeCanvas( cb = null ) {
        var height = this.canvasElement.parentNode.parentNode.clientHeight-1;
        var width = this.canvasElement.parentNode.parentNode.clientWidth;
        fabricCanvas.setHeight(height);
        fabricCanvas.setWidth(width);
        fabricCanvas.renderAll();
        this.setState({height:height, width:width}, cb)
    }

    getCanvasHeight(){
        return this.props.height;
    }

    getCanvasWidth(){
        return this.props.width;
    }
    getActualCanvasLeft(){
        if (this.boundaryRectangle){
            return Math.floor(this.boundaryRectangle.left);
        }
    }

    getActualCanvasTop(){
        if (this.boundaryRectangle){
            return Math.floor(this.boundaryRectangle.top);
        }
    }
    getActualCanvasWidth(){
        if (this.boundaryRectangle){
            return Math.floor(this.boundaryRectangle.width*this.boundaryRectangle.scaleX);
        }
    }

    getActualCanvasHeight(){
        if (this.boundaryRectangle){
            return Math.floor(this.boundaryRectangle.height*this.boundaryRectangle.scaleY);
        }
    }

    componentDidMount() {
        var el = ReactDOM.findDOMNode(this);

        this.canvasElement = el;

        // var width = (this.drawingContainer.clientWidth) - editorPadding;
        // Here we have the canvas so we can initialize fabric
        // console.log("Initializing canvas");
        fabricCanvas.initialize(el, {
            height: this.props.height ,
            width: this.props.width, 
            backgroundColor: "transparent",
            enableRetinaScaling:false,   
            // PreserveObjectStacking:true,
            preserveObjectStacking:true,
            zoom:1
        });

        fabricCanvas.on('mouse:up', () => {
          reactor.dispatch(keys.fabricData, fabricCanvas.toObject());
          reactor.dispatch(keys.activeObject, !!fabricCanvas.getActiveObject());
        });
        
        // an event we will fire when we want to save state
        fabricCanvas.on('saveData', () => {
            reactor.dispatch(keys.fabricData, fabricCanvas.toObject());
            reactor.dispatch(keys.activeObject, !!fabricCanvas.getActiveObject());
            fabricCanvas.renderAll(); // programatic changes we make will not trigger a render in fabric
        });


        

        var boundaryRectangle = new fabric.Rect({
            fill: 'white',
            // originX: 'left',
            // originY: 'top',
            stroke: 'gray',
            strokeWidth:1,
            // strokeDashArray: [1, 1],
            opacity: 0.9,
            width: this.getCanvasWidth()-5,
            height: this.getCanvasHeight()-5,
            selectable:true,
            lockMovementX:true,
            lockMovementY:true,
            class:"boundaryRectangle",
            id:"boundaryRectangle",
            clipFor:'logo',
        });
        // console.log("Boundary rectangle, w =", this.getCanvasWidth(), " h = ", this.getCanvasHeight());

        this.boundaryRectangle = boundaryRectangle; 
        fabricCanvas.add(boundaryRectangle);

        // fabricCanvas.remove(boundaryRectangle);

        // fabricCanvas.clipTo = (ctx) => {
        //   this.boundaryRectangle.render(ctx);
        // };

        var disabled = !this.state.cropMode;
        var r = this.canvasElement.getBoundingClientRect();
        var pos = [0,0];
        pos[0] = r.left;
        pos[1] = r.right;

        var rectangle, isDown, origX, origY;
        fabricCanvas.on("mouse:down",  (event) => {
            if (!this.state.cropMode) return;
            var pointer = fabricCanvas.getPointer(event.e);

            isDown = true;
            origX = pointer.x;
            origY = pointer.y;

            rectangle = new fabric.Rect({
                left: origX,
                top: origY,
                fill: 'transparent',
                stroke: 'gray',
                // stroke: 'transparent',
                strokeWidth: 2,
                objectCaching: false,
                selectable:false,

            });
            this.resizeRectangle = rectangle;
            fabricCanvas.add(rectangle);
            this.setState({crop:true, nothingselected:false});
        });

        fabricCanvas.on("mouse:move",  (event) => {
            if (this.state.crop && this.state.cropMode) {
                var pointer = fabricCanvas.getPointer(event.e);
                if(origX>pointer.x){
                    rectangle.set({ left: Math.abs(pointer.x) });
                }
                if(origY>pointer.y){
                    rectangle.set({ top: Math.abs(pointer.y) });
                }
                
                rectangle.set({ width: Math.abs(origX - pointer.x) });
                rectangle.set({ height: Math.abs(origY - pointer.y) });
                console.log(rectangle);
                fabricCanvas.renderAll();
            }
        });

        fabricCanvas.on("mouse:up",  (event) => {
            this.setState({crop:false});
            fabricCanvas.renderAll();
            fabricCanvas.fire('saveData');
            // console.log(this.resizeRectangle);
        });

        fabricCanvas.fire('saveData');

    }

    increaseXWidth(){
        var increase = 50;
        var totalIncrease = this.state.xIncrease + increase;
        var newwidth = this.boundaryRectangle.width+increase;
        var previouswidth = this.boundaryRectangle.width;
        this.boundaryRectangle.set({width:newwidth});
        this.setState({xIncrease:totalIncrease})
        var zoom = 1-(totalIncrease/(previouswidth-totalIncrease) )*0.8;
        console.log(zoom)
        fabricCanvas.renderAll();
        fabricCanvas.setZoom(zoom)
        return zoom;
    }

    increaseYHeight(){
        var increase = 50;
        var newheight = this.boundaryRectangle.height+increase;
        var totalIncrease = this.state.yIncrease + increase;
        var previousheight = this.boundaryRectangle.height;
        console.log('height =', previousheight)
        this.boundaryRectangle.set({height:newheight})
        this.setState({yIncrease:totalIncrease})
        var zoom = 1-(totalIncrease/(previousheight))*1.1;
        console.log(zoom)
        fabricCanvas.renderAll();
        fabricCanvas.setZoom(zoom)
        return zoom;
    }

    getImageObject(){
        var objs = (fabricCanvas.getObjects());
        for (var i = objs.length - 1; i >= 0; i--) {
            console.log("Found = ", objs[i].get('type'));
            if (objs[i].get('type')=='image'){
                return objs[i];
            }
        }
        return null;
    }

     cropImageFromCanvas(canvas) {
        var ctx = canvas.getContext('2d')
        var w = canvas.width,
        h = canvas.height,
        pix = {x:[], y:[]},
        imageData = ctx.getImageData(0,0,canvas.width,canvas.height),
        x, y, index;

        for (y = 0; y < h; y++) {
            for (x = 0; x < w; x++) {
                index = (y * w + x) * 4;
                if (imageData.data[index+3] > 0) {

                    pix.x.push(x);
                    pix.y.push(y);

                }   
            }
        }
        pix.x.sort(function(a,b){return a-b});
        pix.y.sort(function(a,b){return a-b});
        var n = pix.x.length-1;

        w = pix.x[n] - pix.x[0];
        h = pix.y[n] - pix.y[0];
        var cut = ctx.getImageData(pix.x[0], pix.y[0], w, h);

        canvas.width = w;
        canvas.height = h;
        ctx.putImageData(cut, 0, 0);

        var image = canvas.toDataURL();
        var win=window.open(image, '_blank');
        win.focus();

        }

    trimCanvas( c ) {
        var ctx = c.getContext('2d'),
        copy = document.createElement('canvas').getContext('2d'),
        pixels = ctx.getImageData(0, 0, c.width, c.height),
        l = pixels.data.length,
        i,
        bound = {
          top: null,
          left: null,
          right: null,
          bottom: null
        },
        x, y;
        console.log("pixels length = ",l)
        let jump = 4;
        let jump_2 = 3;

        // c.getContext('2d').scale(window.devicePixelRatio, window.devicePixelRatio);
        for (i = 0; i < l; i += jump) {
        if (pixels.data[i+jump_2] !== 0) {

          x = (i / jump) % (c.width);
          y = ~~((i / jump) / (c.width) );
            // Math.floor(x/2);
            // Math.floor(y/2);
            // console.log(Math.floor(x/2), Math.floor(y/2), c.width)

                // x = x/window.devicePixelRatio;
                // console.log(x, c.width)
          if (bound.top === null) {
            bound.top = y;
          }
          
          if (bound.left === null) {
            bound.left = x; 
          } else if (x < bound.left) {
            bound.left = x;
          }
          
          if (bound.right === null) {
            bound.right = x; 
          } else if (bound.right < x) {
            bound.right = x;
          }
          
          if (bound.bottom === null) {
            bound.bottom = y;
          } else if (bound.bottom < y) {
            bound.bottom = y;
          }
        }
        }
        console.log(bound);
        bound.right -= 8;
        bound.bottom -= 8;
        var trimHeight = bound.bottom - bound.top,
          trimWidth = bound.right - bound.left+3,
          trimmed = ctx.getImageData(bound.left+3, bound.top+3, trimWidth, trimHeight);

        copy.canvas.width = trimWidth;
        copy.canvas.height = trimHeight;
        copy.putImageData(trimmed, 0, 0);
        console.log(copy);
        // open new window with trimmed image:
        return copy.canvas;
    };

    componentWillReceiveProps(newProps){
        console.log("Editor = ", newProps);
    }

    doCrop(){
        // this.boundaryRectangle.set({selectable:false});
        var secondObject = this.getImageObject();
        secondObject.setCoords();
        fabricCanvas.setActiveObject(secondObject);
        var prevZoom = fabricCanvas.getZoom();
        fabricCanvas.setZoom(1);
        this.resizeRectangle.setCoords();
        var zoom = (fabricCanvas.getZoom());
        console.log("Zoom level = ", zoom, "scale =", secondObject.scaleX);
        // zoom = 1;
        var scaleX = 1;
        var scaleY = 1;

        // if (this.boundaryRectangle.scaleX){
        //     scaleX = this.boundaryRectangle.scaleX;
        // }
        // if (this.boundaryRectangle.scaleY){
        //     scaleY = this.boundaryRectangle.scaleY;
        // }

        var newCroppedLeft = ((this.resizeRectangle.left*zoom) -secondObject.left*(1));
        var newCroppedTop = (this.resizeRectangle.top*zoom) -secondObject.top*(1);
        var cropOptions = {
              left: Math.floor(newCroppedLeft),
              top: Math.floor(newCroppedTop),
              width: Math.floor(this.resizeRectangle.width*(scaleX)),
              height: Math.floor(this.resizeRectangle.height*(scaleY)),
              // scaleX:zoom,
              // scaleY:zoom,
           },
        cropDataUrl ;

        cropDataUrl = secondObject.toDataURL(cropOptions); 
        // window.open(cropDataUrl)
        new fabric.Image.fromURL(cropDataUrl, (img) =>{
           fabricCanvas.remove(secondObject, this.resizeRectangle).add(img); //this is your cropped image
           img.set({left:this.resizeRectangle.left, top: this.resizeRectangle.top, selectable:true});
           fabricCanvas.setActiveObject(img);
           fabricCanvas.fire('saveData');
           this.setState({cropMode:false, crop:false}, ()=>{
                fabricCanvas.setZoom(prevZoom);
            });
         });
       this.boundaryRectangle.set({selectable:true});
       
       
    }

    makeAllImagesUnselectable(){
        var objs = (fabricCanvas.getObjects());
        for (var i = objs.length - 1; i >= 0; i--) {
            if (objs[i].get('type')=='image'){
                objs[i].set({selectable:false});
            }
            if (objs[i].get('type')=='rect'){
                objs[i].set({selectable:false});
            }
        }    
    }
    cancelCrop(cb){
        var objs = (fabricCanvas.getObjects());
        for (var i = objs.length - 1; i >= 0; i--) {

            if (objs[i].get('type')=='rect'){
                // objs[i].remove();
                if (objs[i].id != "boundaryRectangle"){
                    fabricCanvas.remove(objs[i]);
                }else{
                    objs[i].set({selectable:true});
                }
                
            }else if (objs[i].get('type')=='image'){
                objs[i].set({selectable:true});
            }
        }
        this.setState({cropMode:false}, cb);
    }


    crop( cb = null ){   

        if (this.state.cropMode){
            if (!this.state.nothingselected){
                this.doCrop();
                if (cb){
                    cb();
                }
            }
        }else{
            this.setState({cropMode:true, nothingselected:true}, ()=>{
                this.makeAllImagesUnselectable();
                if (cb){
                    cb();
                }
            })
        }
    

  }
  render() {
    return <canvas className="editor" ></canvas>
  }
};