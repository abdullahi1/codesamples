import React from 'react';
// import ReactHighcharts from 'react-highcharts'; // Expects that Highcharts was loaded in the code.
import {Spinner,Button} from 'react-mdl';
  // let chartReflow = undefined;
import AppDialog from '../common/AppDialog';
import SystemSelect from './common/SystemSelect';
import TimePeriod from './common/TimePeriod';
import CLChart from './common/Chart';

class CallDistributionToday extends React.Component{
	constructor(props){
		super(props);
		this.api = CLAppAPI;
		
		this.state={
			loading:true,
			modalOpen:false,
			sysSelect: {selected:""},
			timeSelect: {selected:""},
			widgetOptions: {sysSelectText:"", timeSelectText:""},
			chartSeries:[],
		}
		this.mmounted = false;
		
		this.chartConfig= {
			// reflow:true,
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Call Distribution'
		    },
		    subtitle: {
		        text: ''
		    },
		    xAxis: {
		        categories: [
		            '',

		        ],
		        crosshair: true
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Number of Calls'
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y} calls</b></td></tr>',
		        footerFormat: '</table>',
		        shared: false,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		            pointPadding: 0.2,
		            borderWidth: 0
		        }
		    },
	        credits: {
		        enabled: false
		    },			    
		    series: [
			    {
			        name: 'Incoming',
			        data: [0]

			    }, {
			        name: 'Outgoing',
			        data: [0]

			    }
		    ]
		}
		this.config = CLAppConfig;
		this.updateThreadRunning = 0;
		this.loadsPerformed = 0;
		this.logger = CLAppLogger;
		this.utils = CLAppUtils;
		this.changeRequests = 0;
		this.updateEndpoint = "/Widgets/get/name/call-distribution";
		this.staticupdateEndpoint = "/Widgetsstatic/call_distribution";


	}
	
	// handleModalClose() {
	// 	this.props.onjustAddedComplete({close:true,saved:false});
	// };	

	updateChart(data, cb){
	    this.setState({chartSeries:data.series},cb);
	}

	componentWillReceiveProps(newProps){
		this.logger.trace("[CallDistributionToday] received props");
		if (newProps.widgetOpts.isReady){
			this.updateChart(newProps.updatedData);
		}
	}

	getModalContent(){
		return (<div>
			<div className="row">
				<div className="col-xs-3"></div>
				<div className="col-xs-6">
					<SystemSelect 
						ref={(input) => { let sysSelect={name:'sysSelect',ref:input}; this.props.modalRefAdd(sysSelect) }}
					/>
				</div>
				<div className="col-xs-3"></div>
			</div>
			<div className="row">
				<div className="col-xs-3"></div>
				<div className="col-xs-6">
					<TimePeriod 
						ref={(input) => { let timeSelect={name:'timeSelect',ref:input};this.props.modalRefAdd(timeSelect)}}
					/>
				</div>
				<div className="col-xs-3"></div>
			</div>
		</div>)
	}

	componentDidMount(){
		this.logger.trace("[CallDistributionToday] Mounted. Setting Update URL")
		this.props.wrapperInit({updateURL:this.updateEndpoint, staticUpdateURL:this.staticupdateEndpoint,hasModal:true});
		this.mmounted = true;
		if ( this.props.widgetOpts.isReady ){
			this.props.widgetIsReady();
		}
	}

	componentWillUnmount(){
		this.logger.trace("[CallDistributionToday] Unmounting");
		this.mmounted = false;
	}

	getSubttiletext(){
		let subtitle = this.props.widgetOpts.sysSelectText+' - '+this.props.widgetOpts.timeSelectText;
		return subtitle;
	}

	getWidgetContent(){
		if (!this.props.widgetOpts.isReady || this.props.isLoading){
			return <Spinner/>
		}
		return (
			<div>
			 	<CLChart 
			 	chartConfig={this.chartConfig} 
			 	series={this.state.chartSeries}
			 	subtitle={this.getSubttiletext()}
			 	/>
			</div>
			)

	}
	render(){
		return (
			<div style={{height:'420px'}}>
			{this.getWidgetContent()}
			</div>
		)
	}
}

export default Widget1 